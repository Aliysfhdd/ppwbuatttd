


var csrftoken = getCookie('csrftoken');
function getCookie(c_name){
    if (document.cookie.length > 0)
    {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1)
        {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start,c_end));
        }
    }
    return "";
}

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("isi").style.display = "block";
}
function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();
  sessionStorage.setItem("name",profile.getName());
  $.ajax({
		url: '/update' ,
		method: "POST",
		
		data: {
            'name': profile.getName(), 
        },
		success: function(data) {
                    console.log(data);
					
					}
  })
  event.preventDefault();
	
}

var favorite=0;

sessionStorage.bintang = favorite;

function  fav(id){
	var ids= $('#' +id);
	var srcs= ids[0].src;
	 if (srcs.includes('false')) {
        favorite++;
        ids[0].src = '../static/image/star.gif';
    } else {
        favorite--;
        ids[0].src = '../static/image/false.png';
    }
	sessionStorage.setItem('bintang', favorite);
    $('#favi').html('<h3>'+ favorite+ '</h3>');
} 
function pilih(i){
	favorite=0;
	$('#favi').html('<h3>'+ favorite+ '</h3>');
	$('#datas').html("<tr><th id='table'>Cover</th><th id='table'>Title</th><th id='table'>Author</th><th> Fav </th></tr>")
	$.ajax({
		url: '/data/'+i ,
		success: function(obj){
			var i;
			for(i=0; i<obj.items.length; i++){
				var judul= obj.items[i]['volumeInfo']['title'];
				var image= '<img id="zoom" src="' + obj.items[i]['volumeInfo']['imageLinks']['thumbnail'] +'">';
				var author= obj.items[i]['volumeInfo']['authors'];
				$('#datas').append('<tr><td>'+image+'</td><td>'+judul+'</td><td>'+ author+'</td><td><img class="imagess" onclick=fav("'+ obj.items[i].id +'") id='+obj.items[i].id+ ' src="/static/image/false.png" style="height:50px;"></td></tr>');
				
		}
		
		
	}});
}


$ (document).ready(function(){
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});
$( function() {	
	$( "#accordion" ).accordion({
		collapsible: true,
		heightStyle: "content",
        clearStyle: true,
		active: false,	
		});
	} );


	

$.ajax({
	url: '/data/human'	,
	success: function(obj){
		var i;
		for(i=0; i<obj.items.length; i++){
			var judul= obj.items[i]['volumeInfo']['title'];
			var image= '<img id="zoom" src="' + obj.items[i]['volumeInfo']['imageLinks']['thumbnail'] +'">';
			var author= obj.items[i]['volumeInfo']['authors'];
			$('#datas').append('<tr><td>'+image+'</td><td>'+judul+'</td><td>'+ author+'</td><td><img class="imagess" onclick=fav("'+ obj.items[i].id +'") id='+obj.items[i].id+ ' src="/static/image/false.png" style="height:50px;"></td></tr>');
			
			
	}
	
}});

var clicked= true;
$("#button").click(function(){
    if(clicked){
        $('body').css('background-image', 'url("../static/image/minimalism-shooting-stars-4k-7r.jpg")');
		$('body').css('color', '#EEEEEEAA');
		$('body').css('font-family', "'Exo 2', sans-serif");
		$('#accordion h3').css('color', 'rgba(255,255,255,0.7)');
		$('#accordion h3').css('background', 'linear-gradient(to top left, rgba(102, 0, 102, 0.8) 1%, rgba(204, 0, 102, 0.8) 100%')
		$('#accordion h3').css('box-shadow', '0px 0px 0px  rgba(255,255,255,0.5)');
		$('#accordion h3').css('font-family', "'Exo 2', sans-serif");
		$('#accordion div').css('color', 'rgba(255,255,255,0.7)');
		$('#accordion div').css('font-family', "'Exo 2', sans-serif");
		
		
        clicked  = false
    } 
	else {
        $('body').css('background-image', 'url("../static/image/Sunshine To Your Desktop With These Wallpapers Lifehacker Australia.png")');
		$('body').css('color', 'black');
		$('body').css('font-family', "'Short Stack', cursive");
		$('#accordion h3').css('color', 'black');
		$('#accordion h3').css('background', 'rgba(255,255,255,0.2)')
		$('#accordion h3').css('box-shadow', '3px 3px 3px  grey');
		$('#accordion h3').css('font-family', "'Short Stack', cursive");
		$('#accordion div').css('color', 'rgba(0,0,0,0.7)');
		$('#accordion div').css('font-family', "'Short Stack', cursive");
		
		
        clicked  = true;
    }   
});
});
