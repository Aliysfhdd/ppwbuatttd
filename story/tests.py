from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import resolve
from .views import hello,home, jsonreq
from .forms import Forms
from .models import Orang
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class StoryTest(TestCase):
        def test_url_is_exist(self):
            response = Client().get('/')
            self.assertEqual(response.status_code,200)
        def test_url_not_found(self):
            response = Client().get('/huhu/')
            self.assertEqual(response.status_code, 404)
      

        def test_json(self):
            response = Client().get('/data/quilting')
            self.assertEqual(response.status_code,200)
            response = jsonreq(HttpRequest())
            html = response.content.decode('utf8')
            self.assertIn('books', html)
            

        def test__using_template(self):
            response = Client().get('/')
            self.assertTemplateUsed(response, 'Home.html')

        def test_using_hello_func(self):
            found = resolve('/')
            self.assertEqual(found.func, home)

        def test_form_validation_max_status_length(self):
            form = Forms(data={'isi': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaaaaaaaaaaaa',})
            self.assertTrue(form.is_valid())

        def test_form_validation_exceed_max_status_length(self):
            form = Forms(data={'isi': 'aAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaaaaaaaaaaaa',})
            self.assertFalse(form.is_valid())
            
        def test_form_validation_blank_status(self):
            form = Forms(data={'isi': '',})
            self.assertFalse(form.is_valid())

        def test_model(self):
            Orang.objects.create( isi='HUHUHU')
            self.assertEqual(Orang.objects.all().count(), 1)

        def test_page_has_content(self):
            response = hello(HttpRequest())
            html = response.content.decode('utf8')
            self.assertIn('Hello, apa kabar', html)

        def test_isi_form(self):
            form=Forms(data={'isi' : "hello",})
            
        def test_success_and_render_the_result(self):
            test = 'FUfufuf'
            response_post = Client().post('/form/',{ 'isi': test})
            self.assertEqual(response_post.status_code, 200)
            response = Client().get('/form/')
            html_response = response.content.decode('utf8')
            self.assertIn(test, html_response)

        def test_error_and_render_the_result(self):
            response_post = Client().post('/form/',{ 'isi': ''})
            self.assertEqual(response_post.status_code, 200)
            response = Client().get('/form')
            html_response = response.content.decode('utf8')
            self.assertNotIn('hehe', html_response)

        def test_profile_page(self):
            response= home(HttpRequest())
            html = response.content.decode('utf8')
            self.assertIn('Nama saya', html)
            self.assertIn('Ali Yusuf',html)
            self.assertIn('Anak UI',html)

        def test_get_form_function(self):
            response = Client().get('/form/')
            self.assertEqual(response.status_code, 200)

        def test_using_template(self):
            response = Client().get('/form')
            self.assertTemplateUsed(response, 'hello.html')

##    
##class StoryFunctionalTest(TestCase):
##    def setUp(self):
##        chrome_options = Options()
##        chrome_options.add_argument('--dns-prefetch-disable')
##        chrome_options.add_argument('--no-sandbox')        
##        chrome_options.add_argument('--headless')
##        chrome_options.add_argument('disable-gpu')
##        service_log_path= './chromedriver.log'
##        service_args= ['--verbose']
##        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
##        self.selenium.implicitly_wait(25)
##        super(StoryFunctionalTest, self).setUp()
##
##    def tearDown(self):
##        self.selenium.quit()
##        super(StoryFunctionalTest, self).tearDown()
##
##        
##
##    def test_input_todo(self):
##        selenium = self.selenium
##        selenium.get('http://localhost:8000/form')
##        time.sleep(3)
##        title = selenium.find_element_by_name('isi')
##        title.send_keys('Coba Coba')
##        title.send_keys(Keys.RETURN)
##        time.sleep(5)
##        obj = selenium.find_elements_by_class_name('activity')
##        self.assertEquals( "Coba Coba", obj[-1].text)
##
##  
##    def test_layout1(self):
##        selenium = self.selenium
##        selenium.get('http://ppw-f-huehue.herokuapp.com/')
##        time.sleep(3)
##        self.assertEquals('Ali Yusuf', selenium.title)
##
##    def test_layout2(self):
##        selenium = self.selenium
##        selenium.get('http://ppw-f-huehue.herokuapp.com/Home/experience')
##        time.sleep(3)
##        home = selenium.find_element_by_class_name('navbar-brand')
##        home.click()
##        self.assertIn('Hello..', selenium.page_source)
##
##    def test_css1(self):
##        selenium = self.selenium
##        selenium.get('http://ppw-f-huehue.herokuapp.com/Home/experience')
##        time.sleep(3)
##        home = selenium.find_element_by_class_name('ToggleActive')
##        css= home.value_of_css_property('background-color')
##        self.assertEquals('rgba(255, 209, 143, 1)', css)
##        
##    def test_css2(self):
##        selenium = self.selenium
##        selenium.get('http://ppw-f-huehue.herokuapp.com')
##        time.sleep(3)
##        home = selenium.find_element_by_id('navbar')
##        css= home.value_of_css_property('font-size')
##        self.assertEquals('20px', css)
        
    
        

