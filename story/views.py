from django.contrib.auth import logout
from django.shortcuts import render, redirect
from .forms import Forms
from .models import Orang
from datetime import datetime
from django.http import HttpResponse
import requests 
import json
from django.http import JsonResponse
from django.contrib.sessions.backends.db import SessionStore
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
# Create your views here.
response={}

def hello(request):
    form= Forms(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        isi = request.POST['isi']
        date= datetime.now()
        isian= Orang(isi=isi, date=date)
        isian.save()
        message = Orang.objects.all()
        response['message'] = message
        return render(request, "hello.html", response)
    else:
        response['form']= Forms
        message = Orang.objects.all()
        response['message'] = message
        return render(request, "hello.html", response)

def home(request):
        return render(request, "Home.html")


def library(request):
    if request.user.is_authenticated:
        user=User.objects.get(email=request.user.email)
        social = user.social_auth.get(uid=request.user.email)
        request.session['name']= request.user.first_name
        request.session['token']= social.extra_data['access_token']
        request.session['email']= request.user.email
    else:
        request.session['name']= "My"
    response['name']= request.session['name']
    print(request.session['name'])
    return render(request, "library.html", response)

def logout_view(request):
    logout(request)
    request.session.flush()
    return redirect('story:library')
def login(request):
    return redirect('story:library')


##@csrf_exempt
##def update(request):
##    nama = request.POST.get('name', None)
##    request.session['name']= nama
##    request.session.save()
##    return render(request, "library.html", response)


def jsonreq(request, pilih=None):
    url = requests.get("https://www.googleapis.com/books/v1/volumes?q="+ str(pilih))
    jsons= url.json()
    return JsonResponse(jsons)
