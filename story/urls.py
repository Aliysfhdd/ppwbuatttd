from django.conf.urls import url
from django.urls import path
from .views import hello, home, library, jsonreq, login, logout_view
app_name= 'story'
urlpatterns = [
    url(r'^$', home, name='home'),
    url(r'^form', hello, name='hello'),
    url(r'^library', library, name='library'),
    url(r'^logout', logout_view, name='logout'),
    url(r'^login', login, name='login'),
    path('data/<pilih>', jsonreq),
]
