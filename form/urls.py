from django.conf.urls import url
from django.urls import path
from .views import homes, validate_mail, renders, tampil, jsonreq, remove
app_name= 'form'
urlpatterns = [
    url(r'^$', renders, name='render'),
    url(r'^submit', homes, name='home'),
    url(r'^validate-mail', validate_mail, name='validate-mail'),
    url(r'^list', tampil, name='tampil'),
    url(r'^data', jsonreq, name='data'),
    url(r'^remove', remove, name='remove'),
   
]
