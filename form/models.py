from django.db import models

# Create your models here.
class Subs(models.Model):
    fullname = models.CharField(max_length=60)
    email = models.EmailField(max_length=30, unique=True)
    password = models.CharField(max_length=30)
