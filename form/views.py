from django.shortcuts import render, redirect
from django.db import IntegrityError
from django.http import HttpResponse
from .models import Subs
from .forms import SubsForm
from django.http import JsonResponse
from django.db import IntegrityError
import json
import requests 

# Create your views here.
response = {}

def renders(request):
    form = SubsForm(request.POST or None)
    response['form'] = form
    return render(request, 'subs.html', response)

    
def homes(request):
    try:
        if(request.method == "POST"):
            fullname = request.POST.get('fullname', None)
            email = request.POST.get('email', None)
            password = request.POST.get('password', None)
            Subs.objects.create(fullname=fullname, email=email,password=password)
            return HttpResponse(json.dumps({'result': 'success, thank you ' + fullname,
                                            'email': email,
                                            'name': fullname
                                            }))
        else:
            return HttpResponse(json.dumps({'result': 'fail'}))

    except IntegrityError as e:
        return HttpResponse(json.dumps({'result': 'fail'}))


def validate_mail(request):
    mail = request.POST.get('email', None)
    data = {
        'is_taken': Subs.objects.filter(email=mail).exists()
    }
    return JsonResponse(data)

def tampil(request):
    response['isi']= Subs.objects.all()
    return render (request,'list.html', response)

def jsonreq(request):
    sub= Subs.objects.all()
    lists= list()
    for i in sub:
        orang={ 'nama': i.fullname, 'email': i.email}
        lists.append(orang)
    return HttpResponse(json.dumps({'items': lists}))

def remove(request):
    email = request.POST.get('email', None)
    Subs.objects.all().filter(email=email).delete()
    return HttpResponse(json.dumps({'items': email}))
