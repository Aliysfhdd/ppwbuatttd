var sabi=false;
var csrftoken = getCookie('csrftoken');
function getCookie(c_name){
    if (document.cookie.length > 0)
    {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1)
        {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start,c_end));
        }
    }
    return "";
}

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
function unsub(id){
	var ids =document.getElementById(id);
	ids.remove();
	$.ajax({
		method: "POST",
		url: '/subscribe/remove',
		dataType: 'json',
		data: {
            'email': id, 
        },
	});
} 

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function valid(){
	if(($("#id_email").val()==null || $("#id_email").val()=="") || ($("#id_password").val()==null || $("#id_password").val()=="") || ($("#id_fullname").val()==null || $("#id_fullname").val()=="")){
		return false;
	}
	return true;
}

$ (document).ready(function(){

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

$.ajax({
	url: '/subscribe/data'	,
	dataType: 'json',
	success: function(obj){
		var i;
		for(i=0; i<obj.items.length; i++){
			var nama= obj.items[i]['nama'];
			var email= obj.items[i]['email'];
			$('#data').append('<tr id='+email+'><td>'+nama+'</td><td>'+email+'</td><td><button class="btn btn-info" onclick=unsub("'+email+'")>Unsubscribe</button></td></tr>');		
	}
}});

$("form" ).submit(function(event) {
    $.ajax({
        method: "POST",
		url: '/subscribe/submit',
        
        data: {
			'fullname': $('#id_fullname').val(),
            'email': $("#id_email").val(),
            'password': $("#id_password").val(),
        },
		dataType: 'json',
        success: function(msg) {
			
            $( "#dialog" ).dialog();
			$( "#dialog" ).html("<p>"+msg['result']+"</p>");
			$("#id_password").val('');
			$("#id_username").val('');
			$("#id_email").val('');
			
        },
		error: function(msg){
			alert('Email NOT Sent');
        },
		
    });
	event.preventDefault();
});

$("#id_password").change(function () {
	if(valid() && sabi){
		$("#submit")[0].disabled=false;
	}	
	else{
		$("#submit")[0].disabled=true;
	}	
});

$("#id_fullname").change(function () {
	if(valid() && sabi){
		$("#submit")[0].disabled=false;
	}
	else{
		$("#submit")[0].disabled=true;
	}	
});

$("#id_email").change(function () {
        email = $(this).val();
        $.ajax({
            method: "POST",
            url: '/subscribe/validate-mail',
            data: {
                'email': email
            },
            dataType: 'json',
            success: function (data) {
				
				if(isEmail(email)==false){
					$('#id_email').css("border-color", "red");
					$("#submit")[0].disabled=true;
					$('#peringatan').html('Masukan Email yang benar!');
					sabi=false;
					
				}
                else if (data.is_taken) {
                    alert("Email sudah terdaftar!");
					$('#id_email').css("border-color", "red");
					$("#submit")[0].disabled=true;
					$('#peringatan').html('Silahkan menggunakan email lain');
					sabi= false;
                }
				else{
					if(valid()){
						$("#submit")[0].disabled=false;						
					}
					sabi=true;
					$('#id_email').css("border-color", "white");
					$('#peringatan').html('');
				}
            }
        });
	})



})
