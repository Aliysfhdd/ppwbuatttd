from django import forms
from .models import Subs

class SubsForm(forms.Form):
    fullname = forms.CharField(label = "Name", required = True, max_length=60, widget=forms.TextInput())
    email = forms.EmailField(label= "Email", required = True, max_length=30)
    password = forms.CharField(label = "Password", max_length=30, required=True,widget=forms.PasswordInput())
    
