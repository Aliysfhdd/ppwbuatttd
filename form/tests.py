from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import resolve
from .views import homes, validate_mail, jsonreq, remove
from .forms import SubsForm
from .models import Subs
from django.db import IntegrityError

class FormTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/subscribe/')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, 'subs.html')
    def test_url_is_exist1(self):
        response = Client().get('/subscribe/list')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, 'list.html')

    def test_url_is_exist3(self):
        response = Client().get('/subscribe/data')
        self.assertEqual(response.status_code,200)
        Subs.objects.create( fullname="ali", email="ahasdhs@gmadi.com", password="aas")
        response = jsonreq(HttpRequest())
        html = response.content.decode('utf8')
        self.assertIn('items', html)
        self.assertIn('ali', html)    

    def test_url_is_exist4(self):
        response = Client().get('/subscribe/remove')
        self.assertEqual(response.status_code,200)
        response = remove(HttpRequest())
        html = response.content.decode('utf8')
        self.assertIn('items', html)
    
    def test_url_is_exist2(self):
        response = Client().get('/subscribe/validate-mail')
        self.assertEqual(response.status_code,200)
        response = validate_mail(HttpRequest())
        html = response.content.decode('utf8')
        self.assertIn('false', html)


    def test_post(self):
        name = 'heh'
        email= 'aa@gmail.com'
        password='aa'
        response_post = Client().post('/subscribe/submit',{ 'fullname': name, 'email':email, 'password':password})
        self.assertEqual(response_post.status_code, 200)

    
    def test_post2(self):
        name = 'heh'
        email= ''
        password='aa'
        response_post = Client().get('/subscribe/submit',{ 'fullname': name, 'email':email, 'password':password})
        self.assertEqual(response_post.status_code, 200)
        

    def test_postf(self):
        response_post = Client().post('/subscribe/submit',{ 'fullname': 'a', 'email':'b@gmail.cpm', 'password':'a'})
        response_post1 = Client().post('/subscribe/submit',{ 'fullname': 'a', 'email':'b@gmail.cpm', 'password':'a'})
        self.assertEqual(response_post1.status_code, 200)
        
        
